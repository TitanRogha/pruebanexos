package com.pruebajava.rest.personasservice.dto;


import lombok.Data;

import java.util.Date;

@Data
public class Familia {
    private Long id;
    private String nombres;
    private Double miembros;
    private String status;
    private Date createAt;
    private Date modifyAt;


}
