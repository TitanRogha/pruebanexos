package com.pruebajava.rest.personasservice.controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pruebajava.rest.personasservice.entities.RequestAudit;
import com.pruebajava.rest.personasservice.exceptions.ErrorMessage;
import com.pruebajava.rest.personasservice.entities.Persona;
import com.pruebajava.rest.personasservice.exceptions.PersonaNotFoundException;
import com.pruebajava.rest.personasservice.services.PersonaService;
import org.apache.coyote.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//Se declara clase Controladora de Servicio Rest

@RestController
@RequestMapping(value =("/v1/personas"))
public class PersonaController {

    private static final Logger logger = LoggerFactory.getLogger(PersonaController.class);

    //Inyectamos dependencias
    @Autowired
    private PersonaService personaService;



    //Declaramos nuestro primer metodo en el que devolvemos todas las personas creadas sin filtro
    @GetMapping
    public ResponseEntity<List<Persona>> listPersona(HttpServletRequest request){
        List<Persona> personas;
        personas=personaService.listAllPersona();
        //Se llama metodo asíncrono para guardar auditoría
        personaService.requestAudit(request);

        if(personas.isEmpty()){
            //Se llama metodo asinctrono para guardar auditoria de excepciones
            personaService.exceptionAudit("PersonasNotFound",request.getMethod(),Long.parseLong("0"),"Persona no encontrada");
            return ResponseEntity.notFound().build();

        }

        //Se llama método asincrono para guardar auditoria de response
        personaService.responseAudit(String.valueOf(ResponseEntity.ok(personas).getHeaders()),String.valueOf(ResponseEntity.ok(personas).getBody()),request.getMethod());

        return ResponseEntity.ok(personas);

    }

    //Metodo para obtener persona por id
    @GetMapping(value="/{id}")
    public ResponseEntity<Persona> getPersona(@PathVariable("id") Long id,HttpServletRequest request){
        Persona persona = personaService.getPersona(id);
        personaService.requestAudit(request);

        if (persona==null){
            personaService.exceptionAudit("PersonaNotFound",request.getMethod(),id,"Persona no Encontrada");
            throw new PersonaNotFoundException(id);

        }
        personaService.responseAudit(String.valueOf(ResponseEntity.ok(persona).getHeaders()),String.valueOf(ResponseEntity.ok(persona).getBody()),request.getMethod());
        return ResponseEntity.ok(persona);
    }
    //Metodo para crear persona
    @PostMapping
    public ResponseEntity<Persona> createPersona(@Valid @RequestBody Persona persona, BindingResult result,HttpServletRequest request){
        String jsonString = persona.toString();
        personaService.requestAudit(request);

        if (result.hasErrors()){
            personaService.exceptionAudit("BadRequest",request.getMethod(),Long.parseLong("0"),this.formatMessage(result));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,this.formatMessage(result));

        }
        Persona personaCreate = personaService.createPersona(persona);
        personaService.responseAudit(String.valueOf(ResponseEntity.ok(personaCreate).getHeaders()),String.valueOf(ResponseEntity.ok(personaCreate).getBody()),request.getMethod());
        return ResponseEntity.status(HttpStatus.CREATED).body(personaCreate);
    }
    //Metodo para modificar persona por id
    @PutMapping(value="/{id}")
    public ResponseEntity<Persona> updatePersona(@PathVariable("id")Long id,@RequestBody Persona persona,HttpServletRequest request) {
        persona.setId(id);
        Persona personaDB = personaService.updatePersona(persona);
        personaService.requestAudit(request);
        if (personaDB == null) {
            personaService.exceptionAudit("PersonaNotFound",request.getMethod(),id,"Persona No Encontrada");
            throw new PersonaNotFoundException(id);
        }
        personaService.responseAudit(String.valueOf(ResponseEntity.ok(personaDB).getHeaders()),String.valueOf(ResponseEntity.ok(personaDB).getBody()),request.getMethod());

        return ResponseEntity.ok(personaDB);
    }
    //Meotodo para eliminar una persona por id
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Persona> deletePersona(@PathVariable("id")Long id,HttpServletRequest request){
        Persona personDelete = personaService.deletePersona(id);
        personaService.requestAudit(request);
        if(personDelete==null){
            personaService.exceptionAudit("PersonaNotFound",request.getMethod(),id,"Persona No Encontrada");
            throw new PersonaNotFoundException(id);
        }
        personaService.responseAudit(String.valueOf(ResponseEntity.ok(personDelete).getHeaders()),String.valueOf(ResponseEntity.ok(personDelete).getBody()),request.getMethod());

        return ResponseEntity.ok(personDelete);
    }
    //Creación de metodo para formatear resultado de las validaciones de Hibernate y mostrarlas en Json
    private String formatMessage(BindingResult result){
        List<Map<String,String>> errors = result.getFieldErrors().stream()
                .map(err ->{
                    Map<String,String> error = new HashMap<>();
                    error.put(err.getField(),err.getDefaultMessage());
                    return error;
                }).collect(Collectors.toList());

        ErrorMessage errorMessage = ErrorMessage.builder()
                .code("01").messages(errors).build();

        ObjectMapper mapper = new ObjectMapper();
        String jsonString="";
        try{
            jsonString=mapper.writeValueAsString(errorMessage);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    };










}
