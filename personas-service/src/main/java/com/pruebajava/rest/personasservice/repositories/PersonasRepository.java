package com.pruebajava.rest.personasservice.repositories;

import com.pruebajava.rest.personasservice.entities.Persona;
import org.springframework.data.jpa.repository.JpaRepository;


//Repositorio extendiendo Jpa y Crud con el fin de registrar auditoria de excepciones

public interface PersonasRepository extends JpaRepository<Persona,Long> {



}
