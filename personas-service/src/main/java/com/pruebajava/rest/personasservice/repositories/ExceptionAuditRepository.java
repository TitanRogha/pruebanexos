package com.pruebajava.rest.personasservice.repositories;

import com.pruebajava.rest.personasservice.entities.Exceptions;
import org.springframework.data.jpa.repository.JpaRepository;


//Repositorio extendiendo Jpa y Crud de personas
public interface ExceptionAuditRepository extends JpaRepository<Exceptions,Long> {
}
