package com.pruebajava.rest.personasservice.exceptions;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;


//Clase Entidad que se usa para Mapear codigos de respuesta validados por Hibernate Validator
@Data
@Builder
public class ErrorMessage {
    private String code;
    private List<Map<String,String>> messages;

}
