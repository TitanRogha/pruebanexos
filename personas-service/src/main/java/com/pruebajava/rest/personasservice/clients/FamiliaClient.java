package com.pruebajava.rest.personasservice.clients;


import com.pruebajava.rest.personasservice.dto.Familia;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name="familias-service")
@RequestMapping(value ="/v1/familias")
public interface FamiliaClient {
    @PostMapping(value="/members/{id}")
    public ResponseEntity<Familia> updateMiembros(@PathVariable("id") Long id, Double quantity);
}
