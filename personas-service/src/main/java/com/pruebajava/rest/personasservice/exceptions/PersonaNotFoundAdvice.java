package com.pruebajava.rest.personasservice.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


//Controller Advice que mapea mensaje de error y setea Code 404 para cuando no se encuentran personas
@ControllerAdvice
public class PersonaNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String personaNotFoundHandler(PersonaNotFoundException ex){
        return ex.getMessage();
    }
}
