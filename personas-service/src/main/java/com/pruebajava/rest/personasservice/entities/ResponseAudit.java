package com.pruebajava.rest.personasservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="tbl_audit_response_persons")
//Se crean anotaciones de proyecto lombok para obtener getters,setters,constructores,toString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ResponseAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String header;
    private String body;
    @Column(name="fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;
}
