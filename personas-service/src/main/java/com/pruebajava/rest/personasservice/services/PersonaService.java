package com.pruebajava.rest.personasservice.services;

import com.pruebajava.rest.personasservice.entities.Exceptions;
import com.pruebajava.rest.personasservice.entities.Persona;
import com.pruebajava.rest.personasservice.entities.RequestAudit;
import com.pruebajava.rest.personasservice.entities.ResponseAudit;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.CompletableFuture;

//Clase Interfaz para definir los comportamientos del Servicio de personas
public interface PersonaService {
    public List<Persona> listAllPersona();
    public Persona getPersona(Long id);
    public Persona createPersona(Persona persona);
    public Persona updatePersona(Persona persona);
    public Persona deletePersona(Long id);
    public RequestAudit requestAudit(HttpServletRequest request);
    public Exceptions exceptionAudit(String tipo, String metodo, Long id, String mensaje);
    public ResponseAudit responseAudit(String headers, String body, String metodo);

}
