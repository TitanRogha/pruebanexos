package com.pruebajava.rest.personasservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Entity
@Table(name="tbl_rol_persona")
@AllArgsConstructor @NoArgsConstructor @Builder @Data
public class RolPersona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotEmpty(message="La descripcion no puede estar vacío")
    private String descripcion;

}

