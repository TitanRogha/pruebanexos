package com.pruebajava.rest.personasservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

//Entidad para grabar logs de auditoria

@Entity
@Table(name="tbl_audit_request_persons")
//Se crean anotaciones de proyecto lombok para obtener getters,setters,constructores,toString
@AllArgsConstructor @NoArgsConstructor @Builder @Data
public class RequestAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String method;
    private String origen;
    private String header;
    private String body;
    @Column(name="fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;

}
