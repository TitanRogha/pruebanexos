package com.pruebajava.rest.personasservice.exceptions;


//Clase de Excepcion con mensaje de error genérico
public class PersonaNotFoundException extends RuntimeException{
    public PersonaNotFoundException(Long id ) {
        super("No se puede encontrar la persona con id "+id);
    }

}
