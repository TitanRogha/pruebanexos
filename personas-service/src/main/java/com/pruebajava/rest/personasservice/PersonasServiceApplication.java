package com.pruebajava.rest.personasservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;


@SpringBootApplication
@EnableAsync
@EnableEurekaClient
@EnableFeignClients
public class PersonasServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonasServiceApplication.class, args);
	}

	//Creacion de Hilo ejecutor para metodos asincronos limitados a 500 s y 2 cores
	@Bean
	public Executor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(2);
		executor.setMaxPoolSize(2);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("Auditoria-");
		executor.initialize();
		return executor;
	}
}
