package com.pruebajava.rest.personasservice.repositories;

import com.pruebajava.rest.personasservice.entities.RequestAudit;
import org.springframework.data.jpa.repository.JpaRepository;

//Creacion de repositorio extendiendo Jpa que a su vez extiende los CRUD para registro de Auditoria de Request
public interface RequestAuditRepository extends JpaRepository<RequestAudit,Long> {
}
