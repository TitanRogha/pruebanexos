package com.pruebajava.rest.personasservice.entities;

import com.pruebajava.rest.personasservice.dto.Familia;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;

@Entity
@Table(name="tbl_personas")

//Se crean anotaciones de proyecto lombok para obtener getters,setters,constructores,toString
@AllArgsConstructor @NoArgsConstructor @Builder @Data
public class Persona {

//Se establecen atributos del entity Persona y anotaciones de JPA para creacion de primary Key

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//Se establecen anotaciones de validacion de hibernate con sus respectivos mensajes y anotaciones de columnas en base de datos
    @NotEmpty(message="El nombre puede ser vacío")
    @Size(min=2,max=40,message = "El tamaño del nombre es inválido")
    @Column(name="first_name",length=40)
    private String nombre;
    @NotEmpty
    @Size(min=2,max=40,message = "El tamaño del apellido es inválido")
    @Column(name="last_name",length =40)
    private String apellido;
    @Positive(message="La edad tiene que ser mayor a 0")
    @Column(length = 10)
    private int edad;
    @Column(name="family_id")
    @NotNull(message="El id de la Familia no puede estar vacío(Consultar microservicio personas)")
    @Min(0)
    @Max(9999)
    private Long idFamilia;
    private int idPadre;
    @Column(length = 20)
    private String status;
    @NotNull @Positive(message="El numero de documento tiene que ser mayor a 0")
    private int numeroDocumentoidentidad;
    private boolean casado;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name ="rol_persona_id")
    @NotNull(message="el rol de la persona  no puede estar vacío(Esposo,Esposa,Hijo,Hija)")
    private RolPersona rolPersona;
    @Column(name="fecha_registro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;
    @Column(name="fecha_modificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifyAt;

    //Se establece dto en entidad principal con transient para no guardar en bd
    @Transient
    private Familia familia;
}
