package com.pruebajava.rest.personasservice.services;


import com.pruebajava.rest.personasservice.clients.FamiliaClient;
import com.pruebajava.rest.personasservice.controllers.PersonaController;
import com.pruebajava.rest.personasservice.entities.Exceptions;
import com.pruebajava.rest.personasservice.entities.Persona;
import com.pruebajava.rest.personasservice.entities.RequestAudit;
import com.pruebajava.rest.personasservice.entities.ResponseAudit;
import com.pruebajava.rest.personasservice.repositories.ExceptionAuditRepository;
import com.pruebajava.rest.personasservice.repositories.PersonasRepository;
import com.pruebajava.rest.personasservice.repositories.RequestAuditRepository;
import com.pruebajava.rest.personasservice.repositories.ResponseAuditRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;


//Clase Servicio que implementa los metodos de la interfaz
@Service
@RequiredArgsConstructor
public class PersonaServiceImpl implements PersonaService {

    private static final Logger logger = LoggerFactory.getLogger(PersonaController.class);

    //Importe de repositorios
    private final PersonasRepository personasRepository;
    private final RequestAuditRepository requestAuditRepository;
    private final ExceptionAuditRepository exceptionAuditRepository;
    private final ResponseAuditRepository responseAuditRepository;

    @Autowired
    FamiliaClient familiaClient;

    //Declaracion de métodos
    @Override
    public List<Persona> listAllPersona() {
        return personasRepository.findAll();
    }

    @Override
    public Persona getPersona(Long id) {
        return personasRepository.findById(id).orElse(null);
    }

    @Override
    public Persona createPersona(Persona persona) {
        persona.setStatus("Creada");
        persona.setCreateAt(new Date());
        Double defaultMember=1.0;
        familiaClient.updateMiembros(persona.getIdFamilia(),defaultMember);

        return personasRepository.save(persona);
    }

    @Override
    public Persona updatePersona(Persona persona) {
        Persona personaDB=getPersona(persona.getId());
        if(personaDB==null){
            return null;
        }
        personaDB.setNombre(persona.getNombre());
        personaDB.setApellido(persona.getApellido());
        personaDB.setCasado(persona.isCasado());
        personaDB.setEdad(persona.getEdad());
        personaDB.setNumeroDocumentoidentidad(persona.getNumeroDocumentoidentidad());
        personaDB.setStatus("Modificado");
        personaDB.setModifyAt(new Date());
        return personasRepository.save(personaDB);
    }

    @Override
    public Persona deletePersona(Long id) {
        Persona personaDB=getPersona(id);
        if(personaDB==null){
            return null;
        }
        personaDB.setStatus("Eliminada");
        personaDB.setModifyAt(new Date());
        return personasRepository.save(personaDB);
    }


    //Creacion de metodo asincrono de auditoria de datos de ingreso
    @Override
    @Async
    public RequestAudit requestAudit( HttpServletRequest request) {

        RequestAudit requestAudit=new RequestAudit();
        requestAudit.setOrigen(request.getRemoteHost());
        requestAudit.setMethod(request.getMethod());
        requestAudit.setCreateAt(new Date());
        requestAudit.setHeader(request.getHeader("user-agent"));


        return requestAuditRepository.save(requestAudit);
    }

    //Creacion de metodo asincrono de auditoria de datos de excepciones
    @Override
    @Async
    public Exceptions exceptionAudit(String tipo,String metodo, Long id, String mensaje) {

        Exceptions exception=new Exceptions();
        exception.setTipo(tipo);
        exception.setId(id);
        exception.setMensaje(mensaje);
        exception.setMetodo(metodo);
        exception.setCreateAt(new Date());
        return exceptionAuditRepository.save(exception);
    }

    //Creacion de metodo asincrono de auditoria de datos de egreso
    @Override
    @Async
    public ResponseAudit responseAudit(String headers, String body, String metodo) {

        ResponseAudit responseAudit = new ResponseAudit();
        responseAudit.setBody(body);
        responseAudit.setHeader(headers);
        responseAudit.setCreateAt(new Date());

        return responseAuditRepository.save(responseAudit);
    }


}
