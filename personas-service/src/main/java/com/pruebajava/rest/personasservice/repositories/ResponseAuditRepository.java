package com.pruebajava.rest.personasservice.repositories;

import com.pruebajava.rest.personasservice.entities.ResponseAudit;
import org.springframework.data.jpa.repository.JpaRepository;

//Creacion de repositorio extendiendo Jpa que a su vez extiende los CRUD para registro de Auditoria de Responses
public interface ResponseAuditRepository extends JpaRepository<ResponseAudit,Long> {
}
