package com.pruebajava.rest.personasservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="tbl_audit_exceptions_persons")
//Se crean anotaciones de proyecto lombok para obtener getters,setters,constructores,toString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Exceptions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String tipo;
    private String metodo;
    private int idPersona;
    private String mensaje;
    @Column(name="fecha_modificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;

}
