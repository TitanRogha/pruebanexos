package com.pruebajava.rest.familyservice.repositories;

import com.pruebajava.rest.familyservice.entities.Familia;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FamiliaRepository extends JpaRepository<Familia,Long>{



}
