package com.pruebajava.rest.familyservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FamilyserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FamilyserviceApplication.class, args);
	}

}
