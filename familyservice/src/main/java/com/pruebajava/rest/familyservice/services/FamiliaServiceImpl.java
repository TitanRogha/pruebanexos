package com.pruebajava.rest.familyservice.services;

import com.pruebajava.rest.familyservice.entities.Familia;
import com.pruebajava.rest.familyservice.repositories.FamiliaRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FamiliaServiceImpl implements FamiliaService{

    private static final Logger logger = LoggerFactory.getLogger(FamiliaServiceImpl.class);

    private final FamiliaRepository familiaRepository;

    @Override
    public List<Familia> listAllFamilia() {
        return familiaRepository.findAll();
    }

    @Override
    public Familia getFamilia(Long id) {
        return familiaRepository.findById(id).orElse(null);
    }

    @Override
    public Familia createFamilia(Familia familia) {
        familia.setStatus("Creada");
        familia.setCreateAt(new Date());
        familia.setModifyAt(new Date());
        return familiaRepository.save(familia);
    }

    @Override
    public Familia updateFamilia(Familia familia) {
        Familia familiaDB = getFamilia(familia.getId());
        if(familiaDB==null){
            return null;
        }
        familiaDB.setNombres(familia.getNombres());
        familiaDB.setStatus("Modificada");
        familiaDB.setModifyAt(new Date());
        return familiaRepository.save(familiaDB);
    }

    @Override
    public Familia deleteFamilia(Long id) {
        Familia familiaDB=getFamilia(id);
        if(familiaDB==null){
            return null;
        }
        familiaDB.setStatus("Eliminada");
        familiaDB.setModifyAt(new Date());
        return familiaRepository.save(familiaDB);
    }

    @Override
    public Familia updateMembers(Long id, Double quantity) {
        Familia familiaDB=getFamilia(id);
        if(familiaDB==null){
            return null;
        }
        logger.info(String.valueOf(familiaDB.getMiembros()));
        logger.info(String.valueOf(quantity));

        Double members = familiaDB.getMiembros() + quantity;
        familiaDB.setMiembros(members);
        return familiaRepository.save(familiaDB);
    }
}
