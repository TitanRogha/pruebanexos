package com.pruebajava.rest.familyservice.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pruebajava.rest.familyservice.entities.Familia;
import com.pruebajava.rest.familyservice.services.FamiliaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value ="/v1/familias")
public class FamiliaController {

    @Autowired
    private FamiliaService familiaService;

    @GetMapping
    public ResponseEntity<List<Familia>> listFamilia(){
        List<Familia> familias;
        familias=familiaService.listAllFamilia();
        if(familias.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(familias);

    }


    @GetMapping(value="/{id}")
    public ResponseEntity<Familia> getFamilia(@PathVariable("id") Long id){
        Familia familia = familiaService.getFamilia(id);
        if(familia==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(familia);
    }

    @PostMapping
    public ResponseEntity<Familia> createFamilia(@Valid @RequestBody Familia familia, BindingResult result){

        if (result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,this.formatMessage(result));
        }

        Familia familiaCreate = familiaService.createFamilia(familia);
        return ResponseEntity.status(HttpStatus.CREATED).body(familiaCreate);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity<Familia> updateFamilia(@PathVariable("id") Long id,@RequestBody Familia familia){
        familia.setId(id);
        Familia familiaDB = familiaService.updateFamilia(familia);
        if(familiaDB==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(familiaDB);
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity<Familia> deleteFamilia(@PathVariable("id") Long id){
        Familia familiaDB=familiaService.deleteFamilia(id);
        if(familiaDB==null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(familiaDB);
    }

    @PostMapping(value="members/{id}")
    public ResponseEntity<Familia> updateMiembros(@PathVariable("id") Long id, Double quantity){
        Double defaultMembers =1.0;
        Familia familiaDB=familiaService.updateMembers(id,defaultMembers);
        if(familiaDB==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(familiaDB);
    }

    private String formatMessage(BindingResult result){
        List<Map<String,String>> errors = result.getFieldErrors().stream()
                .map(err ->{
                    Map<String,String> error = new HashMap<>();
                    error.put(err.getField(),err.getDefaultMessage());
                    return error;
                }).collect(Collectors.toList());

        ErrorMessage errorMessage = ErrorMessage.builder()
                .code("01").messages(errors).build();

        ObjectMapper mapper = new ObjectMapper();
        String jsonString="";
        try{
            jsonString=mapper.writeValueAsString(errorMessage);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    };


}
