package com.pruebajava.rest.familyservice.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Entity
@Table(name="tbl_familias")

@AllArgsConstructor @NoArgsConstructor @Builder @Data
public class Familia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotEmpty(message="El nombre no puede estar vacío")
    private String nombres;
    private Double miembros;
    private String status;
    private Date createAt;
    private Date modifyAt;


}
