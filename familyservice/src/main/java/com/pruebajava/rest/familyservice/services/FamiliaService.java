package com.pruebajava.rest.familyservice.services;

import com.pruebajava.rest.familyservice.entities.Familia;

import java.util.List;

public interface FamiliaService {

    public List<Familia> listAllFamilia();
    public Familia getFamilia(Long id);
    public Familia createFamilia(Familia familia);
    public Familia updateFamilia(Familia familia);
    public Familia deleteFamilia(Long id);
    public Familia updateMembers(Long id,Double quantity);

}
